/**
 * Mad-Advertisement
 * Copyright (C) 2012 Thorsten Marx <thmarx@gmx.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.thorstenmarx.highcharts4j.wicket.highcharts;

import com.thorstenmarx.highcharts4j.api.Chart;
import com.thorstenmarx.highcharts4j.api.options.Align;
import com.thorstenmarx.highcharts4j.api.options.Layout;
import com.thorstenmarx.highcharts4j.api.options.Title;
import com.thorstenmarx.highcharts4j.api.options.VerticalAlign;
import com.thorstenmarx.highcharts4j.api.options.series.LineSeries;
import com.thorstenmarx.highcharts4j.api.options.series.Point;
import java.util.Arrays;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 * @author Thorsten Marx (thmarx@gmx.net)
 */
public class SimpleHighchartsPage extends HighchartsBasePage {

	private static final long serialVersionUID = 1L;

	public SimpleHighchartsPage() {

		LoadableDetachableModel<Chart> chartModel = new LoadableDetachableModel<Chart>() {
			private static final long serialVersionUID = 4624002739887313462L;
			@Override
			protected Chart load() {
				Chart chart = new Chart();

				chart.getOptions().getTitle().setX(-20);
				chart.getOptions().getTitle().setText("Monthly Average Temperature");
				chart.getOptions().getSubTitle().setX(-20);
				chart.getOptions().getSubTitle().setText("Source: WorldClimate.com");
				chart.getOptions().getXAxis().setCategories(Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun",
						"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"));

				chart.getOptions().getYAxis().setTitle(new Title());
				chart.getOptions().getYAxis().getTitle().setText("Temperature (°C)");
				chart.getOptions().getTooltip().setValueSuffix("°C");
				chart.getOptions().getLegend().setAlign(Align.RIGHT);
				chart.getOptions().getLegend().setLayout(Layout.VERTICAL);
				chart.getOptions().getLegend().setVerticalAlign(VerticalAlign.MIDDLE);
				chart.getOptions().getLegend().setBorderWidth(0);

				LineSeries series = new LineSeries();
				series.setName("Tokyo");
				series.addData(new Point<Float>().setY(7.0f)).addData(new Point<Float>().setY(6.9f)).addData(new Point<Float>().setY(9.5f))
						.addData(new Point<Float>().setY(14.5f)).addData(new Point<Float>().setY(18.2f)).addData(new Point<Float>().setY(21.5f))
						.addData(new Point<Float>().setY(25.2f)).addData(new Point<Float>().setY(26.5f)).addData(new Point<Float>().setY(23.3f))
						.addData(new Point<Float>().setY(18.3f)).addData(new Point<Float>().setY(13.9f)).addData(new Point<Float>().setY(9.6f));
				chart.getOptions().addSeries(series);

				series = new LineSeries();
				series.setName("New York");
				series.addData(new Point<Float>().setY(-0.2f)).addData(new Point<Float>().setY(0.8f)).addData(new Point<Float>().setY(5.7f))
						.addData(new Point<Float>().setY(11.3f)).addData(new Point<Float>().setY(17.0f)).addData(new Point<Float>().setY(22.0f))
						.addData(new Point<Float>().setY(24.8f)).addData(new Point<Float>().setY(24.1f)).addData(new Point<Float>().setY(20.1f))
						.addData(new Point<Float>().setY(14.1f)).addData(new Point<Float>().setY(8.6f)).addData(new Point<Float>().setY(2.5f));
				chart.getOptions().addSeries(series);
				return chart;
			}
		};
		
		
		add(new ChartPanel("chart", chartModel));

	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.highcharts4j.wicket.highcharts;

import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.highcharts4j.api.Chart;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class ChartTest {
	
	@Test
	public void defaultChart () {
		Chart chart = new Chart();
		
		String js = chart.toJs("theID");
		
		Assertions.assertThat(js).isNotNull();
		System.out.println(js);
	}
}

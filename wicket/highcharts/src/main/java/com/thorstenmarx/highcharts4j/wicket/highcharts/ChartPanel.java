/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.highcharts4j.wicket.highcharts;

import com.thorstenmarx.highcharts4j.api.Chart;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.resource.JQueryResourceReference;

/**
 *
 * @author marx
 */
public class ChartPanel extends WebMarkupContainer {
	
	IModel<Chart> model;
	
	public ChartPanel(String id, IModel<Chart> model) {
		super(id);
		this.model = model;
		setOutputMarkupId(true);
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);
		
		response.render(JavaScriptHeaderItem.forReference(JQueryResourceReference.get()));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(ChartPanel.class, "highcharts-4.1.9/js/highcharts.js")));
		
		
		response.render(wrapTinyMceSettingsScript(model.getObject().toJs(getMarkupId())));
	}
	
	protected HeaderItem wrapTinyMceSettingsScript(String settingScript) {
		return OnDomReadyHeaderItem.forScript(settingScript);
	}
	
	
	
}

package com.thorstenmarx.highcharts4j.api.options;

import com.alibaba.fastjson.JSONObject;

/**
 *
 * @author marx
 */
public class Tooltip {
	private String valueSuffix;

	public String getValueSuffix() {
		return valueSuffix;
	}

	public void setValueSuffix(String valueSuffix) {
		this.valueSuffix = valueSuffix;
	}
	
	public JSONObject toJson () {
		JSONObject jsonToolTip = new JSONObject();
		
		if (valueSuffix != null) {
			jsonToolTip.put("valueSuffix", valueSuffix);
		}
		return jsonToolTip;
	}
	
	public String toJsonString () {
		return toJson().toJSONString();
	}
}

package com.thorstenmarx.highcharts4j.api.options;

import com.alibaba.fastjson.JSONObject;
import java.util.List;

/**
 *
 * @author marx
 */
public class Axis implements Option {

	private Title title;
	
	private List<String> categories;

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}
	
	@Override
	public JSONObject toJson () {
		JSONObject jsonAxis = new JSONObject();
		
		if (title != null) {
			jsonAxis.put("title", title);
		}
		if (categories != null) {
			jsonAxis.put("categories", categories);
		}
		return jsonAxis;
	}
}

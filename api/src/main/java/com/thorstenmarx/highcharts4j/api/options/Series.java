/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.highcharts4j.api.options;

/**
 *
 * @author marx
 */
public abstract class Series implements Option {
	public enum Type {
		line
	}
	
	public final Type type;
	
	
	public Series () {
		this.type = Type.line;
	}
	public Series (final Type type) {
		this.type = type;
	}
}

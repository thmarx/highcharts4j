/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.highcharts4j.api.options.series;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.highcharts4j.api.options.Series;
import java.util.ArrayList;
import java.util.List;



/**
 *
 * @author marx
 */
public class LineSeries extends Series {

	private String name;
	private List<Point> data;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Point> getData() {
		return data;
	}

	public LineSeries addData(final Point point) {
		if (data == null) {
			data = new ArrayList<>();
		}
		data.add(point);
		
		return this;
	}


	@Override
	public JSONObject toJson() {
		JSONObject jsonLine = new JSONObject();
		
		if (name != null) {
			jsonLine.put("name", name);
		}
		if (data != null && !data.isEmpty()) {
			JSONArray points = new JSONArray();
			data.stream().forEach((point) -> {
				points.add(point.getY());
			});
			jsonLine.put("data", points);
		}
		
		return jsonLine;
	}
}

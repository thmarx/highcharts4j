package com.thorstenmarx.highcharts4j.api;

import com.alibaba.fastjson.JSONObject;

/**
 *
 * @author marx
 */
public class Chart {
	
	private final Options options = new Options();
	
	public Chart () {
		
	}
	
	public Options getOptions () {
		return options;
	}
	
	public String toJs (final String id) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("$(function () {");
		sb.append("$(\"#").append(id).append("\").highcharts(");
		sb.append(options.toJsonString());
		sb.append(");");
		sb.append("});");
		
		return sb.toString();
	}
}

package com.thorstenmarx.highcharts4j.api.options;

import com.alibaba.fastjson.JSONObject;

/**
 *
 * @author marx
 */
public class Title {
	
	private static final String DEFAULT_TEXT = "Chart title";
	private static final Integer DEFAULT_X = 0;
	
	private String text = DEFAULT_TEXT;
	private Integer x = DEFAULT_X;
	private Integer y;
	
	public Title () {
		
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}
	
	public JSONObject toJson () {
		JSONObject jsonTitle = new JSONObject();
		
		if (text != null) {
			jsonTitle.put("text", text);
		} else {
			jsonTitle.put("text", DEFAULT_TEXT);
		}
		if (x != null) {
			jsonTitle.put("x", x);
		} else {
			jsonTitle.put("x", DEFAULT_X);
		}
		if (y != null) {
			jsonTitle.put("y", y);
		}
		
		
		return jsonTitle;
	}
	
	public String toJsonString () {
		return toJson().toJSONString();
	}
}

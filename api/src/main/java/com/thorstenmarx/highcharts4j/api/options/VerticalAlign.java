/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.highcharts4j.api.options;

/**
 *
 * @author marx
 */
public enum VerticalAlign {
	
	TOP("top"),
	MIDDLE("middle"),
	BOTTOM("bottom");
	
	private String value;
	private VerticalAlign (final String value) {
		this.value = value;
	}

	public String value () {
		return value;
	}
}

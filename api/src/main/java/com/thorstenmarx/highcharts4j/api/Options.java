package com.thorstenmarx.highcharts4j.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.highcharts4j.api.options.Axis;
import com.thorstenmarx.highcharts4j.api.options.Legend;
import com.thorstenmarx.highcharts4j.api.options.Series;
import com.thorstenmarx.highcharts4j.api.options.Title;
import com.thorstenmarx.highcharts4j.api.options.Tooltip;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author marx
 */
public class Options {
	
	private final Title title = new Title();
	private final Title subTitle = new Title();
	
	private final Axis xAxis = new Axis();
	private final Axis yAxis = new Axis();
	
	private final Tooltip tooltip = new Tooltip();
	
	private final Legend legend = new Legend();
	
	private List<Series> series = new ArrayList<>();
	
	public Options () {
		
	}
	
	public Title getTitle () {
		return title;
	}
	public Title getSubTitle () {
		return subTitle;
	}

	public Axis getXAxis() {
		return xAxis;
	}

	public Axis getYAxis() {
		return yAxis;
	}

	public Tooltip getTooltip() {
		return tooltip;
	}
	
	public Legend getLegend () {
		return legend;
	}
	public void addSeries (Series series) {
		this.series.add(series);
	}
	
	public String toJsonString () {
		JSONObject jsonOptions = new JSONObject();
		
		jsonOptions.put("title", title.toJson());
		jsonOptions.put("subtitle", subTitle.toJson());
		jsonOptions.put("xAxis", xAxis.toJson());
		jsonOptions.put("yAxis", yAxis.toJson());
		jsonOptions.put("tooltip", tooltip.toJson());
		jsonOptions.put("legend", legend.toJson());
		JSONArray seriesArrray = new JSONArray();
		for (Series series : this.series) {
			seriesArrray.add(series.toJson());
		}
		jsonOptions.put("series", seriesArrray);
		
		return jsonOptions.toJSONString();
	}
}

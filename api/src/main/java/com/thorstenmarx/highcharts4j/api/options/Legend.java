/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.highcharts4j.api.options;

import com.alibaba.fastjson.JSONObject;

/**
 *
 * @author marx
 */
public class Legend implements Option {

	private int borderWidth = 0;	
	
	private Layout layout = Layout.HORIZONTAL;

	private Align align = Align.CENTER;
	
	private VerticalAlign verticalAlign = VerticalAlign.BOTTOM;
			
	
	public Legend() {
	}

	public int getBorderWidth() {
		return borderWidth;
	}

	public void setBorderWidth(int borderWidth) {
		this.borderWidth = borderWidth;
	}

	public Layout getLayout() {
		return layout;
	}

	public void setLayout(Layout layout) {
		this.layout = layout;
	}

	public Align getAlign() {
		return align;
	}

	public void setAlign(Align align) {
		this.align = align;
	}

	public VerticalAlign getVerticalAlign() {
		return verticalAlign;
	}

	public void setVerticalAlign(VerticalAlign verticalAlign) {
		this.verticalAlign = verticalAlign;
	}

	@Override
	public JSONObject toJson() {
		JSONObject jsonLegend = new JSONObject();
		
		jsonLegend.put("align", align.value());
		jsonLegend.put("verticalAlign", verticalAlign.value());
		jsonLegend.put("borderWidth", borderWidth);
		jsonLegend.put("layout", layout.value());
		
		return jsonLegend;
	}
}

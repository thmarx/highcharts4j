/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.highcharts4j.api.options.series;

import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.highcharts4j.api.options.Option;

/**
 *
 * @author marx
 * @param <T>
 */
public class Point<T extends Number> implements Option {
	
	private String name;
	private String color;
	private T x;
	private T y;
	
	public Point () {
		
	}

	public String getName() {
		return name;
	}

	public Point setName(String name) {
		this.name = name;
		return this;
	}

	public String getColor() {
		return color;
	}

	public Point setColor(String color) {
		this.color = color;
		return this;
	}

	public T getX() {
		return x;
	}

	public Point setX(T x) {
		this.x = x;
		return this;
	}

	public T getY() {
		return y;
	}

	public Point setY(T y) {
		this.y = y;
		return this;
	}

	@Override
	public JSONObject toJson() {
		JSONObject jsonPoint = new JSONObject();
		
		if (name != null) {
			jsonPoint.put("name", name);
		}
		if (color != null){
			jsonPoint.put("color", color);
		}
		if (x != null) {
			jsonPoint.put("x", x);
		}
		if (y != null) {
			jsonPoint.put("y", y);
		}
		
		return jsonPoint;
	}
	
	
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.highcharts4j.api.options;

import com.alibaba.fastjson.JSONObject;

/**
 *
 * @author marx
 */
public interface Option {
	public JSONObject toJson ();
	
	default public String toJsonString () {
		return toJson().toJSONString();
	}
}
